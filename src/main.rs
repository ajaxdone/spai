extern crate clap;
extern crate log;
extern crate notify;
extern crate pretty_env_logger;
extern crate reqwest;

use clap::{App, Arg, ArgMatches};
use notify::{watcher, DebouncedEvent, RecursiveMode, Watcher};

use log::{debug, info};

use reqwest::{Client, Method, Url};

use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;

use std::sync::mpsc::channel;
use std::time::Duration;

const SPAI_VERSION: &'static str = env!("CARGO_PKG_VERSION");

fn parse_args<'a>() -> ArgMatches<'a> {
    App::new("spai")
        .version(SPAI_VERSION)
        .author("Stefan Seemayer <mail+spai@semicolonsoftware.de>")
        .about("Simple publishing of available information - monitors a folder and uploads changes to an HTTP API")
        .arg(Arg::with_name("chop")
            .long("chop")
            .short("c")
            .help("Chop off the roots of the specified paths (may lead to ambiguity if more than one path was specified)"))
        .arg(Arg::with_name("debounce")
             .long("debounce")
             .short("d")
             .help("Debounce interval in seconds")
             .takes_value(true)
             )
        .arg(Arg::with_name("path_as_query")
             .long("path-as-query")
             .short("p")
             .help("Specify the location of the update as a query parameter (i.e. after a ?) instead of a path under the API endpoint")
             )
        .arg(Arg::with_name("PATH")
            .help("Provide a folder to monitor")
            .multiple(true)
            .required(true))
        .arg(Arg::with_name("URL")
             .help("API endpoint to notify")
             .required(true))
        .get_matches()
}

fn read_file(path: &str) -> Result<Vec<u8>, std::io::Error> {
    let md = std::fs::metadata(path)?;
    if md.is_file() {
        let mut f = File::open(path)?;
        let mut buf = Vec::new();
        f.read_to_end(&mut buf)?;
        Ok(buf)
    } else {
        Ok(Vec::new())
    }
}

#[derive(Debug)]
enum PublishError {
    Url(reqwest::UrlError),
    Request(reqwest::Error),
}

impl From<reqwest::UrlError> for PublishError {
    fn from(e: reqwest::UrlError) -> Self {
        PublishError::Url(e)
    }
}

impl From<reqwest::Error> for PublishError {
    fn from(e: reqwest::Error) -> Self {
        PublishError::Request(e)
    }
}

fn normalize_path(path: &str, chop_prefixes: &Vec<String>) -> String {
    let mut path = path;

    for cp in chop_prefixes {
        if path.starts_with(cp) {
            path = &path[cp.len()..]
        }
    }

    std::path::Path::new(path)
        .iter()
        .map(|s| s.to_str().unwrap().to_owned().replace(":", ""))
        .filter(|s| (s.len() > 0) && (s != "/") && (s != "\\"))
        .collect::<Vec<_>>()
        .join("/")
}

fn publish_change(
    base: &Url,
    path: &str,
    method: Method,
    content: Vec<u8>,
    paths_as_parameter: bool,
    chop_prefixes: &Vec<String>,
) -> Result<(), PublishError> {
    let path = normalize_path(path, chop_prefixes);
    debug!("Normalized path to {}", path);

    let url = if paths_as_parameter {
        let mut t = base.clone();
        t.set_query(Some(&format!("path={}", path)));
        t
    } else {
        base.join(&path)?
    };
    debug!("Combined URL is {}", url);

    let client = Client::new();

    let res = client
        .request(method.clone(), url.clone())
        .body(content)
        .send()?;

    info!("{} {}: {}", method, url.as_str(), res.status());

    Ok(())
}

#[derive(Debug)]
enum HandlerError {
    IO(std::io::Error),
    Path(PathBuf),
    Publish(PublishError),
}

impl From<std::io::Error> for HandlerError {
    fn from(e: std::io::Error) -> Self {
        HandlerError::IO(e)
    }
}

impl From<PublishError> for HandlerError {
    fn from(e: PublishError) -> Self {
        HandlerError::Publish(e)
    }
}

#[derive(Debug)]
enum WatchError {
    Notify(notify::Error),
    Watch(std::sync::mpsc::RecvError),
    Handler(HandlerError),
}

impl From<notify::Error> for WatchError {
    fn from(e: notify::Error) -> Self {
        WatchError::Notify(e)
    }
}

impl From<HandlerError> for WatchError {
    fn from(e: HandlerError) -> Self {
        WatchError::Handler(e)
    }
}

fn watch<F>(paths: &Vec<String>, debounce: Duration, handler: F) -> Result<(), WatchError>
where
    F: Fn(DebouncedEvent) -> Result<(), HandlerError>,
{
    let (tx, rx) = channel();

    let mut watcher = watcher(tx, debounce)?;

    for p in paths {
        watcher.watch(p, RecursiveMode::Recursive)?;
    }

    loop {
        match rx.recv() {
            Ok(ev) => handler(ev)?,
            Err(e) => return Err(WatchError::Watch(e)),
        }
    }
}

fn main() {
    pretty_env_logger::init();

    let matches = parse_args();
    let debounce: u64 = matches
        .value_of("debounce")
        .unwrap_or("10")
        .parse()
        .unwrap_or(10);

    let path_as_query = matches.is_present("path_as_query");

    let paths: Vec<String> = matches
        .values_of("PATH")
        .unwrap()
        .map(|p| std::fs::canonicalize(p).unwrap().to_str().unwrap().into())
        .collect();

    let url: Url = matches.value_of("URL").unwrap().parse().unwrap();

    let chop = matches.is_present("chop");
    let chop_prefixes = if chop { paths.clone() } else { Vec::new() };

    info!(
        "Watching {} folder(s) and publishing changes to {}",
        paths.len(),
        url
    );

    if let Err(e) = watch(&paths, Duration::from_secs(debounce), |ev| {
        debug!("Event {:?}", ev);

        match ev {
            DebouncedEvent::Create(p) => {
                info!("Create '{:?}'", p);
                let sp = p.to_str().ok_or(HandlerError::Path(p.clone()))?;
                publish_change(
                    &url,
                    sp,
                    Method::POST,
                    read_file(sp)?,
                    path_as_query,
                    &chop_prefixes,
                )?;
                Ok(())
            }
            DebouncedEvent::Write(p) => {
                info!("Write  '{:?}'", p);
                let p = p.to_str().ok_or(HandlerError::Path(p.clone()))?;
                publish_change(
                    &url,
                    p,
                    Method::PUT,
                    read_file(p)?,
                    path_as_query,
                    &chop_prefixes,
                )?;
                Ok(())
            }
            DebouncedEvent::Rename(o, p) => {
                info!("Rename '{:?}' -> '{:?}'", o, p);
                let o = o.to_str().ok_or(HandlerError::Path(o.clone()))?;
                let p = p.to_str().ok_or(HandlerError::Path(p.clone()))?;
                publish_change(
                    &url,
                    o,
                    Method::DELETE,
                    Vec::new(),
                    path_as_query,
                    &chop_prefixes,
                )?;
                publish_change(
                    &url,
                    p,
                    Method::POST,
                    read_file(p)?,
                    path_as_query,
                    &chop_prefixes,
                )?;
                Ok(())
            }
            DebouncedEvent::Remove(p) => {
                info!("Delete '{:?}'", p);
                let p = p.to_str().ok_or(HandlerError::Path(p.clone()))?;
                publish_change(
                    &url,
                    p,
                    Method::DELETE,
                    Vec::new(),
                    path_as_query,
                    &chop_prefixes,
                )?;
                Ok(())
            }
            _ => Ok(()),
        }
    }) {
        println!("Error: {:?}", e);
    }
}
